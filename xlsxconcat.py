#!/usr/bin/python
# coding=UTF-8

"""

"""

__author__ = "Mindaugas Piešina"
__version__ = "0.0.1"
__email__ = "mpiesina@netscape.net"
__status__ = "Prototype"

import sys
import codecs

from xlstree import xlstree

def main():
    # ----------------------------------
    if (len(sys.argv) < 3):
        print("Error: Give input and output file names as parameters")
        sys.exit(2)

    in_flist_fname = sys.argv[1]
    out_fname = sys.argv[2]

    
   

    try:
        with open(in_flist_fname, 'rb') as in_flist_file:
            
            in_flist = in_flist_file.read().splitlines()
    except OSError as err:
        self.last_error = 'Unable to open file %s (%s)' % (in_flist_fname, err)
        return(False)

    main_tree = xlstree()
    
    if (not main_tree.load(in_flist[0])):
        print("Error: " + main_tree.last_error)
        sys.exit(1)

    main_tree.trim()
    main_tree.append_xlsx_sheet()
       
    
    del in_flist[0]

    for name in in_flist:

        tree = xlstree()
    
        if (not tree.load(name)):
            print("Error: " + tree.last_error)
            sys.exit(1)

        tree.trim()
        tree.append_xlsx_sheet()  
           



        main_tree.append_xlsx(tree)

    if (not main_tree.write(out_fname)):
        print("Error: " + main_tree.last_error)
        sys.exit(1)
         

    
    return


    # ----------------------------------
    tree.trim()

    # ----------------------------------
    if (not tree.write(out_fname)):
        print("Error: " + tree.last_error)
        sys.exit(1)


if __name__ == "__main__":
    main()
